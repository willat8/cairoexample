{-# LANGUAGE ForeignFunctionInterface, EmptyDataDecls, OverloadedStrings #-}

import Foreign
import Foreign.C.String
import Foreign.C.Types
import qualified Graphics.Rendering.Cairo.Types as XP
import System.Posix.Types
import Network.HTTP.Simple
import qualified Data.ByteString.Internal as B

foreign import ccall safe "fmemopen"
    c_fmemopen :: Ptr Word8 -> CSize -> CString -> IO (Ptr ())

foreign import ccall safe "fread"
    c_fread :: Ptr Word8 -> CSize -> CSize -> Ptr () -> IO (CSize)

foreign import ccall safe "cairo_image_surface_create_from_png_stream"
    c_cairo_image_surface_create_from_png_stream :: FunPtr (Ptr () -> Ptr Word8 -> CSize -> IO CInt) -> Ptr () -> IO (Ptr XP.Surface)

foreign import ccall safe "wrapper"
    mkReadFromPngStreamForeign ::            (Ptr () -> Ptr Word8 -> CSize -> IO CInt) ->
                                  IO (FunPtr (Ptr () -> Ptr Word8 -> CSize -> IO CInt))

readFromPngStream f b l = do
  c_fread b 1 l f
  return 0

main = do
  bs <- getResponseBody <$> httpBS "http://127.0.0.1/abc.png"
  let (p, _, l) = B.toForeignPtr bs
  withForeignPtr p $ \bs_ptr -> withCString "rb" $ \cs -> do
    file <- c_fmemopen bs_ptr (fromIntegral l) cs
    fp <- mkReadFromPngStreamForeign readFromPngStream
    c_cairo_image_surface_create_from_png_stream fp file

